console.log("Hello")
console.log("")
/*  */
/*  Array Methods */

/* Mutator Methods
    -it seeks to modify the content of an array.
    - are function that mutate an array after
      they are created. These methods manipulate
      original array performing various tasks such as adding or removing
      elements. */
console.log("");

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];
console.log("Current Array: ", fruits)
/* push()
This method adds items to the end of an 
array and changes the original array. 
        Syntax: arrayName.push("Element")
*/
console.log("");
let fruitsLength = fruits.push('Mango');
console.log(fruitsLength)
console.log("Mutated array from push method: ", fruits); // added Mango to fruits
console.log(fruits);
fruits.push("Melon", "Avocado", "Guava")
console.log("Mutated array from push method: ", fruits);

/* pop()
    This method removes the last item of an array and returns it. */

/* Syntax : arrayName.pop() */
console.log("");
let removedFruit = fruits.pop()
console.log("", removedFruit); // removes guava
console.log("Mutated array from pop method: ", fruits);

/*  unshift()
            This method adds an item(s) to the beginning of 
            an array and changes the original array. 
            -returns the length of the array (when presented inside a variable)

            Syntax: arrayName.unshift(elementA)
                    arrayName.unshift(elementA, element B)
*/
console.log("");
fruits.unshift("Lime", "Banana")
console.log("Mutated array from unshift method: ", fruits);


/* shift() 
        This method removes the first 
        item of an array and returns it.
        
        Syntax: arrayName.shift(elementA)
        
        */
console.log("");
let remove1stfruit = fruits.shift()
console.log("Removed using shift: ", remove1stfruit);
console.log("Mutated array from shift method: ", fruits);

/* Splice 
        - it allows remove elements from a specified 
        index number and adds an element
        Syntax: arrayName.splice(startIndex, deleteCount, 
            elementsToBeAdded)*/
console.log("");
let fruitsSplice = fruits.splice(1, 2, "Cherry", "Lychee");
console.log("To be replaced splice:", fruitsSplice)
console.log("Mutated array from splice method: ", fruits);


//Deleting using splice w/o adding elements

console.log("");
let removeSplice = fruits.splice(3, 2);
console.log("Items that were removed using splice: ", removeSplice)
console.log("Mutated array from splice method: ", fruits);


/* 
            sort()
            -it rearranges the array element in alphanumeric
            order.
            Syntax arrayNmae.sort()

*/
// console.log("");
// console.log("Original: ", fruits);
// fruits.sort();
// console.log("Sorted: ", fruits)


console.log("");

let mixedArr = [12, 'May', 36, 94, 'August', 5, 6.3,
    'Sept', 10, 100, 100
];
console.log("🚀 ", mixedArr.sort());



/* reverse ()
-reverses the order or the element
in a array

Syntax : arrayName.reverse()*/
console.log("");

console.log("Original: ", fruits);
fruits.reverse();
console.log("reverse: ", fruits)

console.log("");
// for srting items in the descending order
console.log("Original: ", fruits);
fruits.sort().reverse();

console.log("sort and reverse: ", fruits);


//end of mutator methods

//Mini-Activity

	/*MINI ACTIVITY:
	 - Debug the function which will allow us to list fruits 
     n the fruits array.
	 	-- this function should be able to receive a string.
	 	-- determine if the input fruit name already exist in the 
        fruits array.
	 		*** If it does, show an alert message: "Fruit already 
            listed on our inventory".
	 		*** If not, add the new fruit into the fruits array 
            ans show an alert message: "Fruit is not listed in our 
            inventory."
	 	-- invoke and register a new fruit in the fruit array.
	 	-- log the updated fruits array in the console


	 	function registerFruit () {
	 		let doesFruitExist = fruits.includes(fruitName);

	 		if(doesFruitExst) {
	 			alerat(fruitName "is already on our inventory")
	 		} else {
	 			fruits.push(fruitName);
	 			break;
	 			alert("Fruit is now listed in our inventory")
	 		}
	 	}
	 	
	*/
// console.log("");


// function registerFruit (fruitName) {
//     let doesFruitExist = fruits.includes(fruitName);

//     if(doesFruitExist) {
//         alert(fruitName + " is already on our inventory")
//     } else {
//         fruits.push(fruitName);
        
//         alert(fruitName + " Fruit is now listed in our inventory")
        
//     }
// }
// console.log(fruits );
// registerFruit('Kaimito');

console.log(fruits );

console.log("");

let colors = [];

colors.push('Blue', 'Red' , 'Violet');
console.log(colors);

colors.unshift('Yellow')
console.log("Adds on 1st array",colors);

colors.shift()
console.log("Deletes 1st Array",colors);

colors.pop()
console.log("Deletes last color",colors);

colors.sort()
console.log("Sorts from alphanumeric",colors);

console.log("");
/* 
        Non- Mutators Methods
        - These are functions or methods they do not modify or change
        in an array.
        -does not manipulate the prignal array performing
        various tasks suchs as returning elements from an array and combining arrays and printing 
        output
*/


let countries = ['US','PH','CAN','SG','TH', 'PH', 'FR', 'DE']; 
console.log("🚀 List of Array: ", countries);

/* IndexOf()

        - returns the index number of the firstmatching element
        found in an array. If no match found result will be -1.The search process will be done
        from our 1st element to last element

        Syntax:
        arrayName.indexOf(searchValue)
        arrayName.indexOf(searchValue, fromIndex)

*/
console.log("");
let fristIndex = countries.indexOf('PH');
console.log("🚀 Result of IndexOf", fristIndex)
fristIndex = countries.indexOf('PH', 4);
console.log("🚀 Result of IndexOf", fristIndex)
fristIndex = countries.indexOf('PH', 7);
console.log("🚀 Result of IndexOf", fristIndex)
fristIndex = countries.indexOf('PH', -1);
console.log("🚀 Result of IndexOf", fristIndex)//only index 0 --> indexSize is accepted

console.log("");

/* lastIndexOf
    -returns the index number of the last matching element found in an array
    The search proces is from last element to 1st element
      Syntax:
        arrayName.lastIndexOf(searchValue)
        arrayName.lastIndexOf(searchValue, fromIndex)
    */
   //let countries = ['US','PH','CAN','SG','TH', 'PH', 'FR', 'DE']; 

        console.log("");
let lastIndex = countries.lastIndexOf('PH');
console.log("🚀 Result of LastIndexOf", lastIndex);
lastIndex = countries.lastIndexOf('PH',4);
console.log("🚀 Result of LastIndexOf", lastIndex);


/* Slice

            -portions/slices elements from our array and returns a new array
            Syntax: arrayName.slice(startIndex)
            arrayName.slice(startIndex, endingIndex)

            */

            console.log(countries);

            let slicedArrA = countries.slice(2);

            console.log("🚀 From Slice Method", slicedArrA)
            
            console.log("");
            console.log(countries);
            let slicedArrB = countries.slice(2,5);
            console.log("🚀 From Slice 2,5", slicedArrB)


            console.log("");
            console.log(countries);
            let slicedArrC = countries.slice(-3);
            console.log("🚀 From Slice -3", slicedArrC)
            
            /* toString()
            
                -converts an array to a string separated by a comma.
                -is used tinternally by JS when an array needs to be displayed as
                text (like in HTML ) or when an objext or arrayneeds to be used
                as a string.
                
                syntax:
                arrayName.toString()
                
                
                */

                console.log("");
         let stringArr = countries.toString();
            console.log("🚀 toString Method: ", stringArr)
                
        
/*  concat()
 combines 2 or more arrays and returns the result
        Syntax: ArrayA.concat(ArrayB)
                ArrayA.concat(element)
 */
                console.log("");
 let taskArrayA = ['drink HTML', 'eat Javascript']
 let taskArrayB = ['inhale css', 'exhale sass']
 let taskArrayC = ['get git','be node'];
 console.log("");
 let tasks = taskArrayA.concat(taskArrayB);
 console.log("🚀 from concat arr A and Arr B", tasks)
 console.log("");
 let tasks2 = taskArrayA.concat(taskArrayC,taskArrayB );
 console.log("🚀 from concat arr A and Arr C and Arr B", tasks2)

 let combineTask = taskArrayA.concat("smell express" , "throw react")
 console.log("Arr A + '' ''", combineTask)

 /* join()
  method combines all array elements into a string.

It is similar to toString() method, but here you can specify 
the separator 
instead of the default comma.
Syntax:
arrayName.join(separatorString)
 */
console.log("");
let students = ['Elysha','Gab','Ronel','Jean'];
console.log(students.join())
console.log(students.join(' ')) //space with single quote
console.log(students.join(" - "))


/* Iteration Methods
    -are loops designed to perform
    repetitive tasks in an array.
    -used for manipulating array data resulting in complex tasks
    
    -normally work with a function supplied as an argument
    -aims to evaluate each element in an array
    -
    */
console.log("");

/* 
            forEach()
            -similar
*/

tasks2.forEach(function(task){
console.log("🚀 " ,task)
    
})

/*
	map()
		- iterates on each element and returns a new array with different value depending on the result of the function's operation

	Syntax:
		arrayName.map(function(individualElement) {
			statement
		})
*/

let numbers = [1, 2, 3, 4, 5]

let numberMap = numbers.map(function(number) {
		console.log(number)
		return number*number
})

console.log("Original Array")
console.log(numbers)
console.log("Result of the map method:")
console.log(numberMap)


/*
	every()
		- checks if all elements in an array met the given condition. Returns a true value if all elements meet the condition and false if otherwise

	Syntax:
	 	arrayName.every(function(individualElement) {
			return expression/condition
	 	})

*/

let allValid = numbers.every(function(number) {
	return (number < 3);
})

console.log("Result of every method:")
console.log(allValid)


/*
	some()
	 	- checks if at least one element in the array meet the given condition. Returns a true value if at least one of the elements meets the given condition and false if otherwise.

	 Syntax:
	 	arrayName.some(function(ind){
			return expression/condition
	 	})

*/

let someValid = numbers.some(function(number) {
	return (number < 3) 
})

console.log("Result from some method:")
console.log(someValid)


/*
	filter()
		- returns a new array that contains elements wcich meets the given condition. Return an empty array in no elements were found (that satisfied the given condition)

	Syntax:
		arrayName.filter(function(individualElement){
			return expression/ condition
		})

*/

let filteredValid = numbers.filter(function(number) {
	return (number < 5)
})

console.log("Result of filter method:")
console.log(filteredValid)


/*
	includes()
		- checks if the argument passed can be found in an array
	- can be chained after another method. The result of the first method is used on the second method until all chained methods have been resolved.	

*/


let products = ["mouse", "KEYBOARD", "laptop", "monitor"]

let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes("a")
})


console.log("Result on includes method:")
console.log(filteredProducts)
