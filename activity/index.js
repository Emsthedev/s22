/*
    Create functions which can manipulate our arrays.
    You may change the names on the initial list before you use the array varaibles.
*/

let registeredUsers = [

    "James Jeffries",
    "Maggie Williams",
    "Macie West",
    "Michelle Queen",
    "Angelica Smith",
    "Fernando Dela Cruz",
    "Mike Dy"
];

let friendsList = [];

console.log("");
console.log(" To Register a New User: register('Your Name');");
console.log(" To Register a New Friend in the Registered List: regAFriend('Friend Name');");
console.log(" To Display All Friends in Friend List: displayFriendsList();");
console.log("To Count Current Friends: countFriends();");
console.log("To Delete the Last Friend You Have Added: delFriend();");
console.log("");

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - Invoke and register a new user in the browser console.
        - In the browser console, log the registeredUsers array.

*/


function registerNewUser (regUser) {
    const name = registeredUsers.toString();
    let doesUserExist = name.includes(regUser);

    if(doesUserExist) {
        alert(regUser + " Registration failed. Username already exists!")
    } else {
        registeredUsers.push(regUser);
        
        alert(regUser + " Thank you for registering!")
        console.log(name );
        
    }
}


registerNewUser('Marty McFly');


/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - This function should be able to receive a string.
        - Determine if the input username exists in our registeredUsers array.
            - If it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - If it is not, show an alert window with the following message:
                - "User not found."
        - Invoke the function and add a registered user in your friendsList in the browser console.
        - In the browser console, log the friendsList array in the console.

*/

function regAFriend(getRegToFriend){
    const name = registeredUsers.toString();
    const doesFriendExist = name.includes(getRegToFriend);

     if(doesFriendExist) {
         friendsList.push(getRegToFriend);
        alert( "You have added " + getRegToFriend + " as a friend!")
    } else {
       
        
        alert(getRegToFriend + " is not found.")
        
    }


}

/*  "James Jeffries",
    "Maggie Williams",
    "Macie West",
    "Michelle Queen",
    "Angelica Smith",
    "Fernando Dela Cruz",
    "Mike Dy" */

regAFriend('Marty McFly');


regAFriend('James Jeffries');


regAFriend('Maggie Williams');

/*
regAFriend('Macie West');


regAFriend('Angelica Smith');


regAFriend('Michelle Queen');






/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/

    function displayFriendsList (friend){
      let showFriend = friendsList[friend]
     
      if(friendsList.length >= 1){
        console.log("");
        console.log("Your Current Friends: ")
            friendsList.forEach((friend) => {
                console.log(friend);
              });
              console.log("");

        }
        else{
            alert("You currently have 0 friends. Add one first.");

        }



    }
    displayFriendsList();
   




    
// STRETCH GOALS

/*
    4. Create a function which will display the amount of registered users in your 
    friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function in the browser console.
*/
function countFriends(friend) {
    if(friendsList.length >= 1){
        
            console.log("You currently have " + friendsList.length +" friends.");
            console.log("");
          }
     else{
        alert("You currently have 0 friends. Add one first.");
     }
}
countFriends();


/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - In the browser console, log the friendsList array.

*/

function delFriend(friend) {
    showFriend = friendsList[friend]
    if(friendsList.length >= 1){
        
       friendsList.pop();
       console.log("Your Current Friends: ")
       friendsList.forEach((friend) => {
        console.log(friend);
      });
       
          }
     else{
        alert("You currently have 0 friends. Add one first.");
     }
}
delFriend();




/*======================================================================================*/



// Try this for fun:
/*
   

    Instead of only deleting the last registered user in the friendsList
     delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/








